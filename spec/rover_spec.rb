require "spec_helper"

describe Rover do
  let(:plateau) { Plateau.new(10, 10)}

  subject { described_class.new(0, 0, "N", plateau)}

  describe "#heading" do
    it "returns the current heading" do
      expect(subject.heading).to eq("N")
    end
  end

  describe "#position" do
    it "returns the current position as an X, Y, heading array" do
      expect(subject.position).to eq([0, 0, "N"])
    end
  end

  describe "#move" do
    it "moves the rover following its heading" do
      expect(subject.position).to eq([0, 0, "N"])

      subject.move
      expect(subject.position).to eq([0, 1, "N"])
    end
  end

  describe "#turn_right" do
    it "spins the rover" do
      expect(subject.heading).to eq("N")
      subject.turn_right
      expect(subject.heading).to eq("E")
      subject.turn_right
      expect(subject.heading).to eq("S")
      subject.turn_right
      expect(subject.heading).to eq("W")
      subject.turn_right
      expect(subject.heading).to eq("N")
    end
  end

  describe "#turn_left" do
    it "spins the rover" do
      expect(subject.heading).to eq("N")
      subject.turn_left
      expect(subject.heading).to eq("W")
      subject.turn_left
      expect(subject.heading).to eq("S")
      subject.turn_left
      expect(subject.heading).to eq("E")
      subject.turn_left
      expect(subject.heading).to eq("N")
    end
  end

  describe "#execute" do
    it "executes a sequence of movement operations" do
      subject.execute("MMRMM")

      expect(subject.position).to eq([2, 2, "E"])
    end

    it "won't move past the plateau boundaries" do
      subject.execute("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")

      expect(subject.position).to eq([0, 10, "N"])

    end
  end
end
