require "spec_helper"

describe Game do
  describe "#results" do
    it "returns an array with the final rovers' positions" do
      game = Game.new("5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM")

      expect(game.results).to eq([[1, 3, "N"],[5, 1, "E"]])
    end
  end
end
