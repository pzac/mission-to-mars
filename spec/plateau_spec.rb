require "spec_helper"

describe Plateau do
  subject do
    described_class.new(10, 20)
  end

  describe "valid_coordinates?" do
    it "checks whether a point is within the plateau boundaries" do
      expect(subject.valid_coordinates?(0, 0)).to be true
      expect(subject.valid_coordinates?(5, 10)).to be true
      expect(subject.valid_coordinates?(5, 25)).to be false
      expect(subject.valid_coordinates?(10, 21)).to be false
    end
  end
end
