require_relative 'lib/game'

input_file = ARGV[0]

unless input_file && File.exists?(input_file)
  puts "Please specify a valid file"
  exit
end

data = File.read(input_file)

game = Game.new(data)

game.results.each.with_index do |line, i|
  puts "Rover ##{i}: #{line}"
end
