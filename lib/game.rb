require 'readline'
require_relative 'plateau'
require_relative 'rover'

class Game
  # Expects input data as a string
  #
  def initialize(data)
    @rovers = []

    lines = data.split(/(\r)?\n/).compact
    parse(lines)
  end

  def results
    @rovers.map(&:position)
  end

  private

  def parse(lines)
    width, height = lines.shift.split.map(&:to_i)
    @plateau = Plateau.new(width, height)

    lines.each_slice(2) do |item|
      @rovers << add_rover(item)
    end
  end

  def add_rover(data)
    setup, operations = data

    x, y, heading = setup.split

    rover = Rover.new(x, y, heading, @plateau)
    rover.execute(operations)

    rover
  end
end
