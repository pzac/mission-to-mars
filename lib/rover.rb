class Rover
  COMPASS_POINTS = %w[N E S W].freeze

  def initialize(x, y, heading, plateau)
    @x = x.to_i
    @y = y.to_i
    @plateau = plateau

    raise "Invalid heading: #{heading}" unless COMPASS_POINTS.include?(heading)

    # Store heading as an index to take advantage of mod operator
    #
    @heading_index = COMPASS_POINTS.index(heading)
  end

  # Returns the heading as a character
  #
  def heading
    COMPASS_POINTS[@heading_index]
  end

  # Moves one step ahead (unless we're hitting the plateau boundaries)
  #
  def move
    x, y = case heading
             when 'N' then [@x, @y + 1]
             when 'E' then [@x + 1, @y]
             when 'S' then [@x, @y - 1]
             when 'W' then [@x - 1, @y]
           end
    if @plateau.valid_coordinates?(x, y)
      @x, @y = x, y
    end

    position
  end

  def turn_left
    @heading_index = (@heading_index - 1) % 4

    position
  end

  def turn_right
    @heading_index = (@heading_index + 1) % 4

    position
  end

  # Runs a sequence of movements expressed as a string
  #
  def execute(operations)
    operations.each_char do |op|
      operation(op)
    end
  end

  def position
    [@x, @y, heading]
  end

  def inspect
    "#<Rover:#{object_id} X: #{@x} Y: #{@y} Heading: #{heading}>"
  end

  private

  # Single movement operation. Returns the new position. Unknown operations are nop-ed
  #
  def operation(op)
    case op
    when 'M' then move
    when 'L' then turn_left
    when 'R' then turn_right
    end

    position
  end
end
