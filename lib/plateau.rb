class Plateau
  # Plateau origin is [0, 0], upper right corner is [width, height].
  #
  def initialize(width, height)
    @width = width
    @height = height
  end

  def valid_coordinates?(x, y)
    x.between?(0, @width) && y.between?(0, @height)
  end
end
